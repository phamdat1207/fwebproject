<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\subject;

class SearchDataController extends Controller
{
    //
    public function create() 
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $data = new subject();
        $data->name=$request->get('tenMH');
        $data->save();
        return redirect('index')->with('success', 'Data has been added Please Now Search Over Here');
    }

    public function index()
    {
        return view('index');
    }

    public function result(Request  $request)
    {
        $result=subject::select('tenMH')
            ->where('tenMH', 'LIKE', "%{$request->input('query')}%")
            ->get();
        $dataModified = array();
        foreach ($result as $data)
        {
            $dataModified[] = $data->tenMH;
        }

        return response()->json($dataModified);
    }
    
}
