<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\subject;
use App\lop;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('createsubjects');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $subject = new subject;
        $subject->tenMH = request('subject_name');
        $subject->maMH = request('subject_id');
        $subject->soTinchi = request('subject_credit');
        $subject->tiet = request('subject_period');
        $subject->HTGD = request('type_training');
        $subject->KhoaQL = '';
        $subject->description = request('type_training');
        $check = subject::where('maMH', $subject->maMH)->first();

        if ($check !== null) {
            return redirect('/createsubjects')->with('error', 'ID already exists');
        } else{
            $subject->save();
            return redirect('/createsubjects');
        }
    }

    public function storeClass(Request $request)
    {
        // return $request->all();
        $class = new lop;
        $class->maNganh = request('class_major');
        $class->maMH = request('class_subject');
        $class->nam = request('class_year');
        $class->HK = request('class_semester');

        $checkMajor = lop::where('maNganh', $class->maNganh)->first();
        $checkSubject = lop::where('maMH', $class->maMH)->first();

        if ($checkMajor !== null & $checkSubject !== null) {
            return redirect('/createclass')->with('error', 'ID major and subject already exists');
        } else{
            $class->save();
            return redirect('/createclass');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showlist()
    {
        //
        $data= subject::paginate(10);
        return view('tableallsubject',['data' => $data]);
    }

    public function showClass()
    {
        //
        $data= DB::table('subjects')->join('lops', 'subjects.maMH', '=', 'lops.maMH')
        ->select('subjects.tenMH', 'subjects.maMH', 'subjects.soTinchi', 'lops.maNganh', 'lops.HK', 'lops.nam')
        ->paginate(10);
        return view('tableclass',['data'=>$data]);
    }

    public function showDisplay(Request $request)
    {
        //
        $year = $request->year;
        $data= DB::table('subjects')->join('lops', 'subjects.maMH', '=', 'lops.maMH')
        ->select('subjects.tenMH', 'subjects.maMH', 'subjects.soTinchi', 'lops.HK', 'lops.nam')
        ->get();
        return view('programdisplay',['data'=>$data, 'year'=>$year]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($maMH)
    {
        //
        $newUpdate = subject::where('maMH', $maMH)->first();
        return view('editsubject', compact('newUpdate'));
    }

    public function editClass($maMH)
    {
        //
        $newUpdate = lop::where('maMH', $maMH)->first();
        return view('editClass', compact('newUpdate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $maMH)
    {
        $descript = "";
        if ($request->subject_description != null) {
            $descript = $request->subject_description;
        }
        
        $news = subject::where(['maMH' => $maMH])->update([
            'tenMH' => $request->subject_name,
            'soTinchi' => $request->subject_credit,
            'tiet' => $request->subject_period,
            'HTGD' => $request->type_training,
            // 'KhoaQL' =>,
            'description' => $descript
        ]);
        return redirect()->action('SubjectController@showlist');
    }

    public function updateClass(Request $request, $maMH)
    {
        $news = lop::where(['maMH' => $maMH])->update([
            'HK' => $request->subject_semester,
            'nam' => $request->subject_year,
        ]);
        return redirect()->action('SubjectController@showClass');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($maMH)
    {
        lop::where(['maMH' => $maMH])->delete();
        return redirect()->action('SubjectController@showClass');
    }

    public function deleteAll(Request $request)
    {
        $maMH = $request->input('checked');
        $dele = lop::whereIn('maMH', $maMH)->delete();
        return redirect()->action('SubjectController@showClass');
    }
}
