<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiangVien extends Model
{
    protected $fillable = [
        'maGV',
        'email',
        'tenGV'
    ];}
