<?php
// app/Repositories/CustomUserRepository.php
namespace App\Repositories;

use App\User;
use App\SinhVien;
use App\GiangVien;
use Auth0\Login\Auth0User;
use Auth0\Login\Auth0JWTUser;
use Auth0\Login\Repository\Auth0UserRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Log;


class CustomUserRepository extends Auth0UserRepository
{

    /**
     * Get an existing user or create a new one
     *
     * @param array $profile - Auth0 profile
     *
     * @return User
     */

    protected function create ($profile) {
        preg_match('/[^@]*$/', $profile['email'], $output_array);
        $checkadmin = $output_array[0];

        preg_match('/[a-zA-Z0-9.-]*/', $profile['email'], $output_array1);
        $id = $output_array1[0];
        if($checkadmin == "it.tdt.edu.vn"){
            $checkadmin = "admin";
            GiangVien::firstOrCreate(['maGV' => $id],[
                'tenGV' => '',
                'email' => $profile['email'] ?? ''
            ]);
        }else{
            $checkadmin = "user";
            SinhVien::firstOrCreate(['maSV' => $id],[
                'tenSV' => '',
                'nienKhoa' => '2018 ',
                'maNganh' => '18H502R',
                'email' => $profile['email'] ?? ''
            ]);
        }
    }

    protected function upsertUser( $profile ) {  
        preg_match('/[^@]*$/', $profile['email'], $output_array);
        $checkadmin = $output_array[0];

        preg_match('/[a-zA-Z0-9.-]*/', $profile['email'], $output_array1);
        $id = $output_array1[0];

        if($checkadmin == "it.tdt.edu.vn"){
            $checkadmin = "admin";
        }else{
            $checkadmin = "user";
        }
        return User::firstOrCreate(['sub' => $profile['sub']], [
            'email' => $profile['email'] ?? '',
            'name' => $profile['name'] ?? '',
            'roles' => $checkadmin ?? '',
        ]);


        
    }

    /**
     * Authenticate a user with a decoded ID Token
     *
     * @param array $decodedJwt
     *
     * @return Auth0JWTUser
     */
    public function getUserByDecodedJWT(array $decodedJwt) : Authenticatable
    {
        $user = $this->upsertUser( (array) $jwt );
        return new Auth0JWTUser( $user->getAttributes() );
    }

    /**
     * Get a User from the database using Auth0 profile information
     *
     * @param array $userinfo
     *
     * @return Auth0User
     */
    public function getUserByUserInfo(array $userinfo) : Authenticatable
    {   
        $user = $this->upsertUser( $userinfo['profile'] );
        $abc = $this->create($userinfo['profile']);
        return new Auth0User( $user->getAttributes(), $userinfo['accessToken'] );
    }

}