<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subject extends Model
{
    protected $fillable = [
        'tenMH', 'maMH', 'soTinchi', 'tiet', 'HTGD', 'KhoaQL', 'description'
    ];
}
