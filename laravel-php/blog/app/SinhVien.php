<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
    protected $fillable = [
        'maSV', 
        'tenSV', 
        'nienKhoa', 
        'maNganh',
        'email'
    ];
}
