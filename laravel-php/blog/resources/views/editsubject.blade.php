<!-- Giao dien nay danh cho Giang Vien (Admin) -->
<!-- Giao dien nay Giang vien se chon de khoi tao mot mon hoc moi -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Teacher</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Data change css here -->
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('css/editsubject.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
</head>
<body>
<!--Nav Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
	<a class="navbar-brand" href="/home">DBSK</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>

	<!--Col-auto start-->
	<div class="col-auto">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
           <li class="nav-item active">
	        	<a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
	    	</li>
		    <li class="nav-item dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="/tablesubject">Subject</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="tableclass">Class</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="/programspecification">Program Specification</a>
		        </div>
			</li>
	    	<li class="nav-item">
	        	<a class="nav-link" href="/aboutus">About Us</a>
			</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/profile">Profile</a>
	    	</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/logout">Log out</a>
	    	</li>
	    </ul>
	</div>
	</div>
	<!--Col-auto end-->
	</div>
</nav>
<!--Nav End-->

<!--Form điền thông tin-->
<div class="container">
    <form id="fm"name="myForm" action="/tablesubject/update/{{$newUpdate->maMH}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <h2>Edit Subject</h2>

        <div class="form-group">
            <label for="subject_id">Subject ID: <strong>{{$newUpdate->maMH}}</strong></label>
        </div>
        
        <div class="form-group">
            <label for="subject_name">Subject name:</label>
            <input type="text" class="form-control" id="subject_name" value="{{$newUpdate->tenMH}}" name="subject_name" required>
        </div>

        <div class="form-group">
            <label for="subject_id">Credit number:</label>
            <input type="number" class="form-control" id="subject_id" value="{{$newUpdate->soTinchi}}" name="subject_credit" required>
        </div>
        
        <div class="form-group">
            <label for="subject_discription">Period:</label>
            <input type="number" class="form-control" id="subject_id" name="subject_period" value="{{$newUpdate->tiet}}">
        </div>

        <div class="form-group">
            <label for="subject_discription">Description:</label>
            <input type="text" class="form-control" id="subject_description" name="subject_description" value="{{$newUpdate->description}}">
        </div>

        <div class="form-group">
            <label for="type_subject">Faculty:</label>
            <select name="type_subject" id="type_subject">
                <option value="CNTT">CNTT</option>
                <option value="1">Laws</option>
                <option value="2">Defen Education</option>
                <option value="3">Major Subject</option>
            </select>
        </div>

        <div class="form-group ">
            <label for="type_training">Type of training:</label>
            <select name="type_training" id="type_training">
                <option value="0"
                @if ($newUpdate->HTGD === 0)
                    {{'selected="selected"'}}
                @endif
                >Vietnamese</option>
                <option value="1" 
                @if ($newUpdate->HTGD === 1)
                    {{'selected="selected"'}}
                @endif
                >English</option>
            </select>
        </div>
        <!-- Dung php bat su kien khi User nhan submit se get du lieu tu form ve -->
        <button type="submit" name="btnsubmit" class="btn btn-default" id="btnEdit">Update</button>
        
    </form>
</div>
<!--Footer Start-->
@extends('footer')
<!--Footer End-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>