<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Data change css here -->
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('css/programdisplay.css') }}">
</head>
<body>
  <!--Nav Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
	<a class="navbar-brand" href="/home">DBSK</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>

	<!--Col-auto start-->
	<div class="col-auto">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	    	<li class="nav-item active">
	        	<a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
	    	</li>
		    <li class="nav-item dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="/tablesubject">All Subject</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="tableclass">Class</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="programspecification">Program Specification</a>
		        </div>
			</li>
	    	<li class="nav-item">
	        	<a class="nav-link" href="/aboutus">About Us</a>
			</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/profile">Profile</a>
	    	</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/logout">Log out</a>
	    	</li>
	    </ul>
	</div>
	</div>
	<!--Col-auto end-->
	</div>
</nav>
<!--Nav End-->

  <div class="Tong">
    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 1</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 1 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 2</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 2 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>

          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 3</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 3 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          
          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 4</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 4 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          
          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 5</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 5 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          
          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 6</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 6 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          
          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 7</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 7 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          
          @endif
        @endforeach
      </table>
    </div>

    <div class="program_flex"> 
      <!-- flex cái này -->
      <table class="tBorder">
        <tr>
          <td>
            <div>Semester 8</div>
          </td>
        </tr>

        @foreach($data as $i)
          @if ($i->HK === 8 & "$i->nam" === $year)
          <tr>
            <td>
              <div class="notBorder">
                <table class="notBorder tbl">
                  <tbody>
                    <tr>
                      <td>{{$i->maMH}}</td>
                      <td>{{$i->soTinchi}}</td>
                    </tr>
                    <tr>
                      <td colspan="2">{{$i->tenMH}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
          
          @endif
        @endforeach
      </table>
    </div>
  </div>

<!--Footer Start-->
@extends('footer')
<!--Footer End-->

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>