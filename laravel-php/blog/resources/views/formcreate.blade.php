<!-- Giao dien nay danh cho Giang Vien (Admin) -->
<!-- Giao dien nay Giang vien se chon de khoi tao mot mon hoc moi -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Teacher</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Data change css here -->
    <link rel="stylesheet" href="{{ asset('css/createsubjects.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
</head>
<body>
<!--Form điền thông tin-->
<div class="container">
    <form name="myForm" action="PopupConfirmSubject.js" method="POST" enctype="multipart/form-data">
        <h2>Create Subject</h2>

        <div class="form-group">
            <!-- Nhap vao ten mon hoc do, bat buoc phai nhap -->
            <label for="subject_name">Subject name:</label>
            <input type="text" class="form-control" id="subject_name" placeholder="Enter subject name" name="subject_name" required>
        </div>

        <div class="form-group">
            <!-- Ma mon hoc se duoc kiem tra xem khi nguoi dung nhap vao co trung voi cai nao da co trong
            database hay khong, neu co se hien thi loi va bat nhap lai -->
            <label for="subject_id">Subject ID:</label>
            <input type="text" class="form-control" id="subject_id" placeholder="Enter subject id" name="subject_id" required>
        </div>
        
        <div class="form-group">
            <!-- Nhap vao mo ta mon hoc, khong bat nuoc phai nhap -->
            <label for="subject_discription">Subject discription:</label>
            <input type="text" class="form-control" id="subject_discription" name="subject_discription">
        </div>

        <div class="form-group">
            <!-- So tin chi, User nhap vao so tin chi cua mon hoc do, bat buoc phai nhap -->
            <label for="subject_credit">Number of credit:</label>
            <input type="text" id="subject_credit" name="subject_credit" required>
        </div>

        <div class="form-group">
        <!-- Mon hoc tu chon, cac kq duoc select se tra ve duoi dang so, 0 => mon dai cuong,
             1 => mon hoc ngoai ngu (English), 2 => mon hoc GDTC va Quoc phong, 3 => mon hoc chuyen nganh -->
            <label for="type_subject">Type of subject:</label>
            <select name="type_subject" id="type_subject">
                <option value="0">General Subject</option>
                <option value="1">Fogein Languague</option>
                <option value="2">Physical Education</option>
                <option value="3">Major Subject</option>
            </select>
        </div>

        <div class="form-group ">
        <!-- Hinh thuc dao tao, co 2 loai la CLC va Tieu chuan -->
            <label for="type_training">Type of training:</label>
            <select name="type_training" id="type_training">
                <option value="standard">Standard</option>
                <option value="high_quality">High-quality</option>
            </select>
        </div>
        <!-- Dung php bat su kien khi User nhan submit se get du lieu tu form ve -->
        <button type="submit" name="btnsubmit" class="btn btn-default">Create</button>
    </form>
</div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>