<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Subject Table</title>
    <!-- Boostrap content here -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- Data change CSS here -->
    <link rel="stylesheet" href="{{ asset('css/tableallsubject.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
</head>
<body>
<!--Nav Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
	<a class="navbar-brand" href="home">DBSK</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>

	<!--Col-auto start-->
	<div class="col-auto">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
           <li class="nav-item active">
	        	<a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
	    	</li>
		    <li class="nav-item dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="/tablesubject">Subject</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="tableclass">Class</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="/programspecification">Program Specification</a>
		        </div>
			</li>
	    	<li class="nav-item">
	        	<a class="nav-link" href="/aboutus">About Us</a>
			</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/profile">Profile</a>
	    	</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/logout">Log out</a>
	    	</li>
	    </ul>
	</div>
	</div>
	<!--Col-auto end-->
	</div>
</nav>
<!--Nav End-->
    <div class="contentTable">
        <div class="content">
            <table>
                <tr>
                    <!-- <th><input type="checkbox" onclick="checkAllbox(this, 'childBox')"></th> -->
                    <th>Subject</th>
                    <th>ID</th>
                    <th>Credit number</th>
                    <th>Period</th>
                    <th>Teaching Languague</th>
                    <th>Faculty</th>
                    <th></th>
                </tr>
                @foreach($data as $i)
                <tr>
                    <!-- <td><input type="checkbox" id="childBox"></td> -->
                    <td>{{$i->tenMH}}</td>
                    <td>{{$i->maMH}}</td>
                    <td>{{$i->soTinchi}}</td>
                    <td>{{$i->tiet}}</td>
                    <td>
                    @if($i->HTGD === 0)
                        Vietnamese
                    @elseif($i->HTGD === 1)
                        English
                    @endif
                    </td>
                    <td>{{$i->KhoaQL}}</td>
                    <td>
                        <!-- <a href="/table/editsubject/edit/{{$i->maMH}}">Edit </a>| -->
                        <a href="/tablesubject/editsubject/edit/{{$i->maMH}}">Edit</a>
                        <!-- <a href="/table/delete/{{$i->maMH}}"> Delete</a> -->
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class="mybtn">
        <button type="submit" class="btn btn-info">
            <a href="/createsubjects">
            Create
            </a>
        </button>
    </div>
    
    <div class="setlink">
    {{ $data->links() }}
    </div>

<!--Footer Start-->
@extends('footer')
<!--Footer End-->
</body>
</html>