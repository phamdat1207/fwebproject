
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .search-container button:hover {
            background: #ccc;
            text-align: center;
        }
        #Search-bar{
            width: 800px;
        }

    </style>
</head>
<body>
    <div>
        <hr>
        <button>Create subject</button>
    </div>
    <div class="content">
        @yield('content')
        <hr>
        @yield('menu')
    </div>

    <div class="search-container">
        <form action="" style="display: flex; margin: 400px 20%;">
          <input id="Search-bar" type="text" placeholder="Search.." name="search" style="text-align: center;">
          <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</body>
</html>