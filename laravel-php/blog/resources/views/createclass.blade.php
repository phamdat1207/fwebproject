<!-- Giao dien nay danh cho Giang Vien (Admin) -->
<!-- Giao dien nay Giang vien se chon de khoi tao mot mon hoc moi -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Teacher</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Data change css here -->
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('css/createsubjects.css') }}">
</head>
<body>
<!--Nav Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
    <a class="navbar-brand" href="/home">DBSK</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!--Col-auto start-->
    <div class="col-auto">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
	        	<a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
	    	</li>
		    <li class="nav-item dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="/tablesubject">Subject</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="/tableclass">Class</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="/programspecification">Program Specification</a>
		        </div>
			</li>
	    	<li class="nav-item">
	        	<a class="nav-link" href="/aboutus">About Us</a>
			</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/profile">Profile</a>
	    	</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/logout">Log out</a>
	    	</li>
        </ul>
    </div>
    </div>
    <!--Col-auto end-->
    </div>
</nav>
<!--Nav End-->

<!--Form điền thông tin -->
<!-- csrf protection passdata through token -->
<div class="container">
    <form name="myForm" action="/createclass" method="POST" enctype="multipart/form-data">
        @csrf
        <h2>Create Class</h2>

        <div class="form-group">
            <!-- Nhap vao ten mon hoc do, bat buoc phai nhap -->
            <label for="subject_name">Major ID:</label>
            <input type="text" class="form-control" id="subject_name" placeholder="Enter subject name" name="class_major" required>
        </div>

        <div class="form-group">
            <!-- Ma mon hoc se duoc kiem tra xem khi nguoi dung nhap vao co trung voi cai nao da co trong
            database hay khong, neu co se hien thi loi va bat nhap lai -->
            <label for="subject_id">Subject ID:</label>
            <input type="text" class="form-control" id="subject_id" placeholder="Enter subject id" name="class_subject" required>
        </div>

        <div class="form-group">
            <!-- So tin chi, User nhap vao so tin chi cua mon hoc do, bat buoc phai nhap -->
            <label for="subject_credit">Year:</label>
            <input type="year" style="width:200px" id="subject_credit" name="class_year" required>
        </div>

        <div class="form-group">
            <!-- So tin chi, User nhap vao so tiet cua mon hoc do, bat buoc phai nhap -->
            <label for="subject_credit">Semester:</label>
            <input type="number" id="subject_period" name="class_semester" required>
        </div>

        <!-- Dung php bat su kien khi User nhan submit se get du lieu tu form ve -->
        <button type="submit" name="btnsubmit" class="btn btn-default" id="btnCreate">Create</button>
    </form>
</div>
<!--Footer Start-->
@extends('footer')
<!--Footer End-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    @if(\Session::has('error'))
    <script type="text/javascript">
        alert("{{ Session::get('error') }}");
    </script>
    @endif
</body>
</html>