@extends('welcome')

@section('content')
    <h1>Auth0 <span class="a0-u--red">❤</span> Laravel</h1>
@endsection

@section('menu')
    @auth
        <a href="{{ route('profile') }}" class="btn btn-default">Profile</a>
        <a href="{{ route('logout') }}" id="qsLogoutBtn" class="btn btn-success">Logout</a>
    @else
        <a href="{{ route('login') }}" id="qsLoginBtn" class="btn btn-success">Login/Signup</a>
    @endauth
@endsection