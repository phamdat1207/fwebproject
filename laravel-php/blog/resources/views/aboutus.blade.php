<!DOCTYPE html>
<html>
<head>
	<title>About Us</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/aboutus.css') }}">
	<link rel="stylesheet" href="{{ asset('css/header.css') }}">
</head>
<body>
<!--Nav Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
	<a class="navbar-brand" href="home">DBSK</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>

	<!--Col-auto start-->
	<div class="col-auto">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
			<li class="nav-item active">
	        	<a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
	    	</li>
		    <li class="nav-item dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="/tablesubject">Subject</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="/tableclass">Class</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="/programspecification">Program Specification</a>
		        </div>
			</li>
	    	<li class="nav-item">
	        	<a class="nav-link" href="/aboutus">About Us</a>
			</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/profile">Profile</a>
	    	</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/logout">Log out</a>
	    	</li>
	    </ul>
	</div>
	</div>
	<!--Col-auto end-->
	</div>
</nav>
<!--Nav End-->
	<section id="team">
		<div class="container my-3 py-5 text-center">
			<div class="row mb-5">
				<div class="col">
					<h1>Our Team</h1>
					<p class="mt-3">It is a long established fact that a reader will be readable content of a page when looking at its layout</p>
				</div>
			</div>
			<div class="row">
				<!--Person 1-->
				<div class="col-lg-3 col-md-6">
					<div class="card">
						<div class="card-body">
							<img src="binh2.jpg" alt="image" class="img-fluid rounded-circle w-50 mb-3">
							<h3>Tran Thanh Binh</h3>
							<h5>Team Leader/Founder</h5>
							<div class="d-flex flex-row justify-content-center">
								<div class="p-4">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-instagram"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Person 1 End-->
				<!--Person 2-->
				<div class="col-lg-3 col-md-6">
					<div class="card">
						<div class="card-body">
							<img src="khai.jpg" alt="image" class="img-fluid rounded-circle w-50 mb-3">
							<h3>Nguyen Hoang Khai</h3>
							<h5>CEO/Co-Founder</h5>
							<div class="d-flex flex-row justify-content-center">
								<div class="p-4">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-instagram"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Person 2 End-->
				<!--Person 3-->
				<div class="col-lg-3 col-md-6">
					<div class="card">
						<div class="card-body">
							<img src="dat.jpg" alt="image" class="img-fluid rounded-circle w-50 mb-3">
							<h3>Pham Tien Dat</h3>
							<h5>Senior Developer</h5>
							<div class="d-flex flex-row justify-content-center">
								<div class="p-4">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-instagram"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Person 3 End-->
				<!--Person 4-->
				<div class="col-lg-3 col-md-6">
					<div class="card">
						<div class="card-body">
							<img src="sang.jpg" alt="image" class="img-fluid rounded-circle w-50 mb-3">
							<h3>Nguyen Bao Hoang Sang</h3>
							<h5>Junior Developer</h5>
							<div class="d-flex flex-row justify-content-center">
								<div class="p-4">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</div>
								<div class="p-4">
									<a href="#">
										<i class="fa fa-instagram"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Person 4 End-->
			</div>
		</div>
	</section>
<!--Footer Start-->
@extends('footer')
<!--Footer End-->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>