
<link rel="stylesheet"  href="{{ asset('css/footer.css') }}">
<section class="footer">
	<div class="container tex-center">
		<div class="row"> 
			<div class="col-md-3">
				<h1 class="title">Useful Links</h1>
				<p>Introduce</p>
				<p>Education</p>
				<p>Co-operate</p>
			</div>
			<div class="col-md-3">
				<h1 class="title">Company</h1>
				<p>About Us</p>
				<p>Contact Us</p>
				<p>Career</p>
			</div>
			<div class="col-md-3">
				<h1 class="title">Follow Us On</h1>
				<p><i class="fa fa-facebook-official"></i> Facebook</p> 
				<p><i class="fa fa-youtube-play"></i> Youtube</p>
				<p><i class="fa fa-twitter"></i> Twitter</p>
			</div>
			<div class="col-md-3">
				<h1 class="title">Our Newletter</h1>
				<div class="border"></div>
				<p>Enter Your Email to get our new and update.</p>
				<form>
					<input type="text" class="txtb" placeholder="Enter your email">
					<input type="submit" class="btn-submit" value="Submit">
				</form>
			</div>	
		</div>
			<hr>
			<p class="copyright">Made with <i class="fa fa-heart"></i> by DBSK Team</p>
	</div>
</section>