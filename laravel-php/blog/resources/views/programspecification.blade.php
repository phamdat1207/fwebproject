<!-- Giao dien nay danh cho Sinh Vien -->
<!-- Giao dien nay Sinh vien se chon xem so do dao tao -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Education program</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Data change css here -->
    <link rel="stylesheet" href="{{ asset('css/programspecification.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
</head>
<body>
<!--Nav Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
	<a class="navbar-brand" href="/home">DBSK</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>

	<!--Col-auto start-->
	<div class="col-auto">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	    	<li class="nav-item active">
	        	<a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
	    	</li>
		    <li class="nav-item dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Subject</a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        <a class="dropdown-item" href="/tablesubject">All Subject</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="tableclass">Class</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="programspecification">Program Specification</a>
		        </div>
			</li>
	    	<li class="nav-item">
	        	<a class="nav-link" href="/aboutus">About Us</a>
			</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/profile">Profile</a>
	    	</li>
			<li class="nav-item">
	        	<a class="nav-link" href="/logout">Log out</a>
	    	</li>
	    </ul>
	</div>
	</div>
	<!--Col-auto end-->
	</div>
</nav>
<!--Nav End-->
    <div class="program">
        <form method="get" action="/programspecification/programdisplay">
            <div class="SelectformLeft">
                <label for="type_training">Type of education:</label><br>
                <select name="type_training" id="Type_training" class="SelectField">
                    <option value="Standard">Standard</option>
                    <option value="High_quality">High-quality</option>
                </select>
            </div>
            <div class="SelectformCenter">
                <label for="academic_year">Academic year:</label><br>
                <select name="year" id="academic_year" class="SelectField">
                    <!-- tạo database đưa vào đây, load cái db vào cái option này -->
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                </select>
            </div>
            <div class="SelectformRight">
                <label for="major:">Major:</label><br>
                <select name="major:" id="major" class="SelectField">
                    <!-- tạo database đưa vào đây, load cái db vào cái option này -->
                    <?php
                        /*

                        */
                    ?>
                    <option value="Software_Engineering">Software Engineering</option>
                    <option value="Business_Administration">Business Administration</option>
                </select>
            </div>
            
            <div class="SelectformButton">
                <button type="submit" name="btnsubmit" class="btn"><h6>Show Program Specifications</h6></button>
            </div>
        </form>
    </div>

<!--Footer Start-->
@extends('footer')
<!--Footer End-->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>