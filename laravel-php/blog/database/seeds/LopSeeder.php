<?php

use Illuminate\Database\Seeder;

class LopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lops = [
            [
                'maMH' => '501031',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],
            [
                'maMH' => '501042',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],
            [
                'maMH' => '501032',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],
            [
                'maMH' => 'D01001',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],
            [
                'maMH' => 'D02030',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],
            [
                'maMH' => 'L00001',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],
            [
                'maMH' => '300015',
                'maNganh' => '18H502R',
                'HK' => '1',
                'nam' => '2018'
            ],[
                'maMH' => '302053',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => '502061',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => '501043',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => '502044',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => 'D02028',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => 'D02029',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => '300051',
                'maNganh' => '18H502R',
                'HK' => '2',
                'nam' => '2018'
            ],
            [
                'maMH' => '301001',
                'maNganh' => '18H502R',
                'HK' => '3',
                'nam' => '2018'
            ],
            [
                'maMH' => '501044',
                'maNganh' => '18H502R',
                'HK' => '3',
                'nam' => '2018'
            ],
            [
                'maMH' => '502046',
                'maNganh' => '18H502R',
                'HK' => '3',
                'nam' => '2018'
            ],
            [
                'maMH' => '502047',
                'maNganh' => '18H502R',
                'HK' => '3',
                'nam' => '2018'
            ],
            [
                'maMH' => '300085',
                'maNganh' => '18H502R',
                'HK' => '3',
                'nam' => '2018'
            ],
            [
                'maMH' => 'L00021',
                'maNganh' => '18H502R',
                'HK' => '3',
                'nam' => '2018'
            ],
            [
                'maMH' => '502051',
                'maNganh' => '18H502R',
                'HK' => '4',
                'nam' => '2018'
            ],
            [
                'maMH' => '301002',
                'maNganh' => '18H502R',
                'HK' => '4',
                'nam' => '2018'
            ],
            [
                'maMH' => '502042',
                'maNganh' => '18H502R',
                'HK' => '4',
                'nam' => '2018'
            ],
            [
                'maMH' => '502056',
                'maNganh' => '18H502R',
                'HK' => '4',
                'nam' => '2018'
            ],
            [
                'maMH' => '301003',
                'maNganh' => '18H502R',
                'HK' => '5',
                'nam' => '2018'
            ],
            [
                'maMH' => '502045',
                'maNganh' => '18H502R',
                'HK' => '5',
                'nam' => '2018'
            ],
            [
                'maMH' => '503040',
                'maNganh' => '18H502R',
                'HK' => '5',
                'nam' => '2018'
            ],
            [
                'maMH' => 'L00025',
                'maNganh' => '18H502R',
                'HK' => '5',
                'nam' => '2018'
            ],
            
            
               
        ];
        foreach ($lops as $lop) {
            DB::table('lops')->insert([
                'maMH' => $lop['maMH'],
                'maNganh' => $lop['maNganh'],
                'nam' => $lop['nam'],
                'HK' => $lop['HK']
            ]);
        }

    }
}
