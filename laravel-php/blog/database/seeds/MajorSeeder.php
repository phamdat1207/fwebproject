<?php

use Illuminate\Database\Seeder;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $majors = [
            [
                'maNganh' => '18H502R',
                'tenNganh' => 'Kỹ thuật phần mềm',
                'he' => 'CLC',
            ]
        ];
        foreach ($majors as $major) {
            DB::table('majors')->insert([
                'maNganh' => $major['maNganh'],
                'tenNganh' => $major['tenNganh'],
                'he' => $major['he'],
            ]);
        }
    }
}
