<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            [
                'maMH' => '301001',
                'tenMH' => 'Những nguyên lý cơ bản của chủ nghĩa Mác - Lênin',
                'soTinchi' => '5',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'XHNV'
            ],
            [   
                'maMH' => '301002',
                'tenMH' => 'Tư tưởng Hồ Chí Minh',
                'soTinchi' => '2',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'XHNV'
            ],
            [
                'maMH' => '301003',
                'tenMH' => 'Đường lối cách mạng của Đảng Cộng sản Việt Nam',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'XHNV'
            ],
            [   
                'maMH' => '302053',
                'tenMH' => 'Pháp luật đại cương',
                'soTinchi' => '2',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'Luật'
            ],
            [
                'maMH' => '501031',
                'tenMH' => 'Giải tích ứng dụng cho Công nghệ thông tin',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '501032',
                'tenMH' => 'Đại số tuyến tính cho Công nghệ thông tin',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502061',
                'tenMH' => 'Xác suất và thống kê ứng dụng cho Công nghệ thông tin',
                'soTinchi' => '5',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '001212',
                'tenMH' => 'Natural English 2',
                'soTinchi' => '5',
                'tiet' => '87',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [
                'maMH' => '001213',
                'tenMH' => 'NGlobal Citizen English 3',
                'soTinchi' => '8',
                'tiet' => '123',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [   
                'maMH' => '001214',
                'tenMH' => 'Global Citizen English 4',
                'soTinchi' => '8',
                'tiet' => '123',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [
                'maMH' => '001215',
                'tenMH' => 'Global Citizen English 5',
                'soTinchi' => '9',
                'tiet' => '159',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [   
                'maMH' => '001201',
                'tenMH' => 'Tiếng Anh 1',
                'soTinchi' => '5',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [
                'maMH' => '001202',
                'tenMH' => 'Tiếng Anh 2',
                'soTinchi' => '5',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [   
                'maMH' => '001203',
                'tenMH' => 'Tiếng Anh 3',
                'soTinchi' => '2',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [
                'maMH' => '001204',
                'tenMH' => 'Tiếng Anh 4',
                'soTinchi' => '5',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [   
                'maMH' => '001205',
                'tenMH' => 'Tiếng Anh 5',
                'soTinchi' => '2',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [
                'maMH' => '001206',
                'tenMH' => 'Tiếng Anh 6',
                'soTinchi' => '5',
                'tiet' => '75',
                'HTGD' => '0',
                'KhoaQL' => 'TDT CLC'
            ],
            [   
                'maMH' => '300015',
                'tenMH' => 'Phương pháp học đại học',
                'soTinchi' => '1',
                'tiet' => '10',
                'HTGD' => '0',
                'KhoaQL' => 'XHNV'
            ],
            [
                'maMH' => 'L00001',
                'tenMH' => 'Kỹ năng phát triển bền vững',
                'soTinchi' => '2',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [   
                'maMH' => '300051',
                'tenMH' => 'Kỹ năng làm việc nhóm',
                'soTinchi' => '1',
                'tiet' => '10',
                'HTGD' => '0',
                'KhoaQL' => 'XHNV'
            ],
            [
                'maMH' => '300085',
                'tenMH' => 'Kỹ năng viết và trình bày',
                'soTinchi' => '1',
                'tiet' => '12',
                'HTGD' => '0',
                'KhoaQL' => 'XHNV'
            ],
            [   
                'maMH' => 'L00021',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Thái độ sống 2',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [
                'maMH' => 'L00025',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Thái độ sống 3',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [   
                'maMH' => 'L00027',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Tư duy phản biện',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [
                'maMH' => 'L00028',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Kỹ năng ra quyết định',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [   
                'maMH' => 'L00022',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Thực tập chuyển hóa cảm xúc EQ',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [
                'maMH' => 'L00023',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Xây dựng Team & lãnh đạo',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [   
                'maMH' => 'L00024',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Khởi nghiệp',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [
                'maMH' => 'L00017',
                'tenMH' => 'Kỹ năng phát triển bền vững - Lãnh đạo chính mình',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [   
                'maMH' => 'L00018',
                'tenMH' => 'Kỹ năng phát triển bền vững - Xác định mục tiêu cuộc đời',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [
                'maMH' => 'L00019',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Thái độ sống 1',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [   
                'maMH' => 'L00020',
                'tenMH' => 'Những kỹ năng thiết yếu cho sự phát triển bền vững - Kỹ năng 5S và Kaizen',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'Bộ môn kỹ năng'
            ],
            [
                'maMH' => 'D01001',
                'tenMH' => 'Bơi lội',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01101',
                'tenMH' => 'GDTC 1 - Bóng đá',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01102',
                'tenMH' => 'GDTC 1 - Taekwondo',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01103',
                'tenMH' => 'GDTC 1 - Bóng chuyền',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01104',
                'tenMH' => 'GDTC 1 - Cầu lông',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01105',
                'tenMH' => 'GDTC 1 - Thể dục',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01106',
                'tenMH' => 'GDTC 1 - Quần vợt',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01120',
                'tenMH' => 'GDTC 1 - Thể hình Fitness',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01121',
                'tenMH' => 'GDTC 1 - Hatha Yoga',
                'soTinchi' => '0',
                'tiet' => '15',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01201',
                'tenMH' => 'GDTC 2 - Karate',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01202',
                'tenMH' => 'GDTC 2 - Vovinam',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01203',
                'tenMH' => 'GDTC 2 - Võ cổ truyền',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01204',
                'tenMH' => 'GDTC 2 - Bóng rổ',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01205',
                'tenMH' => 'GDTC 2 - Bóng bàn',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D01206',
                'tenMH' => 'GDTC 2 - Cờ vua vận động',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [   
                'maMH' => 'D01220',
                'tenMH' => 'GDTC 2 - Khúc côn cầu',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'KHTT'
            ],
            [
                'maMH' => 'D02030',
                'tenMH' => 'Giáo dục quốc phòng - Học phần 3',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'TTQPAN'
            ],
            [   
                'maMH' => 'D02028',
                'tenMH' => 'Giáo dục quốc phòng - Học phần 1',
                'soTinchi' => '0',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'TTQPAN'
            ],
            [
                'maMH' => 'D02029',
                'tenMH' => 'Giáo dục quốc phòng - Học phần 2',
                'soTinchi' => '0',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'TTQPAN'
            ],
            [   
                'maMH' => '501042',
                'tenMH' => 'Phương pháp lập trình',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '501043',
                'tenMH' => 'Cấu trúc dữ liệu và giải thuật 1',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '501044',
                'tenMH' => 'Cấu trúc rời rạc',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502044',
                'tenMH' => 'Tổ chức máy tính',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502046',
                'tenMH' => 'Nhập môn Mạng máy tính',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502047',
                'tenMH' => 'Nhập môn hệ điều hành',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502042',
                'tenMH' => 'Toán tổ hợp và đồ thị',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502051',
                'tenMH' => 'Hệ cơ sở dữ liệu',
                'soTinchi' => '0',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502045',
                'tenMH' => 'Công nghệ phần mềm',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503040',
                'tenMH' => 'Phân tích và thiết kế giải thuật',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504074',
                'tenMH' => 'Kiến tập công nghiệp',
                'soTinchi' => '4',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502043',
                'tenMH' => 'Cấu trúc dữ liệu và giải thuật 2',
                'soTinchi' => '4',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502050',
                'tenMH' => 'Phân tích và thiết kế yêu cầu',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502057',
                'tenMH' => 'Nguyên lý ngôn ngữ lập trình',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503056',
                'tenMH' => 'Phát triển phần mềm trên nền tảng tiến hóa',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503057',
                'tenMH' => 'Công nghệ phần mềm trên nền tảng ứng dụng hiện đại',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503058',
                'tenMH' => 'Hệ thống hình thức và luận lý',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503073',
                'tenMH' => 'Lập trình web và ứng dụng',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503074',
                'tenMH' => 'Phát triển ứng dụng di động',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '504048',
                'tenMH' => 'Xử lý dữ liệu lớn',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504058',
                'tenMH' => 'Kiểm thử phần mềm',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '504060',
                'tenMH' => 'Kiểm chứng và thẩm định phần mềm',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504070',
                'tenMH' => 'Kiến trúc hướng dịch vụ',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '504076',
                'tenMH' => 'Phát triển trò chơi',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504077',
                'tenMH' => 'Mẫu thiết kế',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '505051',
                'tenMH' => 'Nhập môn các hệ thống phân tán',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '505053',
                'tenMH' => 'Thẩm định phần mềm tự động',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '505054',
                'tenMH' => 'Kỹ thuật thiết kế và đặc tả hình thức',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '505055',
                'tenMH' => 'Thiết kế phần mềm nhúng',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '504045',
                'tenMH' => 'Nhập môn xử lý ngôn ngữ tự nhiên',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504073',
                'tenMH' => 'Chuyên đề Công nghệ phần mềm',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '505060',
                'tenMH' => 'Nhập môn Xử lý ảnh số',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '505041',
                'tenMH' => 'Nhập môn xử lý tiếng nói',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503045',
                'tenMH' => 'Truy hồi thông tin',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502048',
                'tenMH' => 'Nhập môn tính toán đa phương tiện',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503079',
                'tenMH' => 'Lý thuyết mật mã',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504087',
                'tenMH' => 'Điện toán đám mây',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503108',
                'tenMH' => 'Thiết kế giao diện người dùng',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503106',
                'tenMH' => 'Lập trình web nâng cao',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503107',
                'tenMH' => 'Phát triển ứng dụng di động nâng cao',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502068',
                'tenMH' => 'IoT cơ bản',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502049',
                'tenMH' => 'Nhập môn Bảo mật thông tin',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '502052',
                'tenMH' => 'Phát triển hệ thống thông tin doanh nghiệp',
                'soTinchi' => '3',
                'tiet' => '30',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502069',
                'tenMH' => 'Quản lý chất lượng dịch vụ và an toàn thông tin',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503043',
                'tenMH' => 'Nhập môn Trí tuệ nhân tạo',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503044',
                'tenMH' => 'Nhập môn Học máy',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503049',
                'tenMH' => 'Nhập môn Bảo mật máy tính',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503066',
                'tenMH' => 'Hệ thống hoạch định nguồn lực doanh nghiệp',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '503067',
                'tenMH' => 'Công nghệ thông tin trong Quản lý quan hệ khách hàng',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '503109',
                'tenMH' => 'Quản trị hệ thống thông tin',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504068',
                'tenMH' => 'Cơ sở dữ liệu phân tán',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '505043',
                'tenMH' => 'Khai thác dữ liệu và Khai phá tri thức',
                'soTinchi' => '3',
                'tiet' => '45',
                'HTGD' => '1',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504078',
                'tenMH' => 'Dự án Công nghệ thông tin 1',
                'soTinchi' => '4',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '512CM1',
                'tenMH' => 'Kỹ năng thực hành chuyên môn',
                'soTinchi' => '0',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '504079',
                'tenMH' => 'Khóa luận tốt nghiệp',
                'soTinchi' => '12',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [   
                'maMH' => '502056',
                'tenMH' => 'Thực tập nghề nghiệp',
                'soTinchi' => '0',
                'tiet' => '45',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ],
            [
                'maMH' => '0301',
                'tenMH' => 'Nhóm tự chọn 1',
                'soTinchi' => '9',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => '0',
            ],
            [   
                'maMH' => '504075',
                'tenMH' => 'Dự án Công nghệ thông tin 2',
                'soTinchi' => '3',
                'tiet' => '0',
                'HTGD' => '0',
                'KhoaQL' => 'CNTT'
            ]
            
            
        ];
        foreach ($subjects as $subject) {
            DB::table('subjects')->insert([
                'maMH' => $subject['maMH'],
                'tenMH' => $subject['tenMH'],
                'soTinchi' => $subject['soTinchi'],
                'tiet' => $subject['tiet'],
                'HTGD' => $subject['HTGD'],
                'KhoaQL' => $subject['KhoaQL']
            ]);
        }
    }
}
