<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            // $table->id();
            $table->timestamps();
            $table->text('tenMH');
            $table->string('maMH');
            $table->integer('soTinchi');
            $table->integer('tiet');
            $table->integer('HTGD');
            $table->string('KhoaQL');
            $table->primary('maMH');
            $table->text('description')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
