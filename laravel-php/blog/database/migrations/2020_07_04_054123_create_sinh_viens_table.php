<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSinhViensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinh_viens', function (Blueprint $table) {
            // $table->id();
            $table->string('maSV');
            $table->text('tenSV');
            $table->year('nienKhoa');
            $table->string('maNganh');
            $table->string('email');
            $table->timestamps();
            $table->primary(['email', 'maNganh', 'maSV']);
            $table->foreign('email')->references('email')->on('users');
            $table->foreign('maNganh')->references('maNganh')->on('majors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinh_viens');
    }
}
