<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('neon');
})->name('neon');

Route::get('/home', function () {
    return view('home');
})->name('home')->middleware(['preventbackbutton','auth']);

Route::get('/createsubjects', function () {
    return view('createsubjects');
})->name('createsubjects')->middleware(['preventbackbutton','auth']);

Route::get('/createclass', function () {
    return view('createclass');
})->name('createclass')->middleware(['preventbackbutton','auth']);

Route::get('/contact', function () {
    return view('contact');
})->name('contact')->middleware(['preventbackbutton','auth']);

Route::get('/table/editsubject/edit/{maMH}', 'SubjectController@edit');

Route::get('/tablesubject',function(){
    return view('tableallsubject');
})->middleware(['preventbackbutton','auth']);

Route::get('/tablesubject/editsubject',function(){
    return view('editsubject');
})->name('editsubject')->middleware(['preventbackbutton','auth']);

Route::get('/aboutus', function () {
    return view('aboutus');
})->name('aboutus')->middleware(['preventbackbutton','auth']);

Route::get('/programspecification', function(){
    return view('programspecification');
})->name('programspecification')->middleware(['preventbackbutton','auth']);

Route::get( '/auth0/callback', '\Auth0\Login\Auth0Controller@callback' )->name( 'auth0-callback' );
Route::get('/login', 'Auth\Auth0IndexController@login' )->name( 'login' );
Route::get('/logout', 'Auth\Auth0IndexController@logout' )->name( 'logout' )->middleware('auth');
Route::get('/profile', 'Auth\Auth0IndexController@profile' )->name( 'profile' )->middleware('auth');

//controller
Route::post('/createsubjects','SubjectController@store');
Route::post('/createclass','SubjectController@storeClass');

Route::get('/tablesubject','SubjectController@showlist');
Route::get('/tableclass','SubjectController@showClass');
Route::PATCH('/tablesubject/update/{maMH}','SubjectController@update');

Route::get('/tableclass/delete/{maMH}','SubjectController@destroy');
Route::get('/tableclass/deleteall','SubjectController@deleteAll');

Route::get('/tablesubject/editsubject/edit/{maMH}','SubjectController@edit');

Route::get('/tableclass/editclass/edit/{maMH}','SubjectController@editClass');
Route::PATCH('/tableclass/update/{maMH}','SubjectController@updateClass');

Route::get('/programspecification/programdisplay', 'SubjectController@showDisplay');

// Route::group(['middleware' => 'App\Http\Middleware\MemberMiddleware'], function()
// {
// 	Route::match(['get', 'post'], '/memberOnlyPage/', 'HomeController@user');
// });
// Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
// {
// 	Route::match(['get', 'post'], '/home', 'HomeController@admin');	
// });

Route::get('create','SearchDataController@create');
Route::post('store','SearchDataController@store');
Route::get('/index','SearchDataController@index');
Route::get('/search','SearchDataController@result');
//admin middleware
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	Route::match(['get', 'post'], '/createsubjects','SubjectController@index');	
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	Route::match(['get', 'post'], '/tablesubject/editsubject/edit/{maMH}','SubjectController@edit');	
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	Route::match(['get', 'post'], '/tableclass/editclass/edit/{maMH}','SubjectController@editClass');	
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	Route::match(['get', 'post'], '/tableclass/delete/{maMH}','SubjectController@destroy');	
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	Route::match(['get', 'post'], '/tableclass/deleteall','SubjectController@deleteAll');	
});